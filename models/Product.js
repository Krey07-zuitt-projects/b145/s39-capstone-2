const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({

	productName: {
		type: String,
		required: [true, "Product name is required"]
	},

	description: {
		type: String,
		required: [true, "description is required"]
	},

	category: {
		type: String,
		required: [true, "category is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: false
	},

	quantity: {
		type: Number,
		default: 0
	}

}, {timestamps: true});

module.exports = mongoose.model('Product', productSchema);