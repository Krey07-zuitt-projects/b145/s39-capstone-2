const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

// USER ROUTERS

// SIGN UP
router.post('/signup', (req, res) => {
	userController.signUp(req.body).then(resultFromController => res.send(resultFromController))
});

// SIGN IN
router.post('/signin', (req, res) => {
	userController.signIn(req.body).then(resultFromController => res.send(resultFromController))
});


// ADMIN ROUTERS

// set as new admin

router.put('/:userId', (req, res) => {

	const data = {
		userId: req.params.userId,
		payload : auth.decode(req.headers.authorization),
		updatedUser: req.body
	}

	userController.changeToAdmin(data).then(resultFromController => res.send(resultFromController))
});


// get all user

router.get('/getuser', auth.verify, (req, res) => {

	const data = {
		payload: auth.decode(req.headers.authorization)
	}

	userController.getUser(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;