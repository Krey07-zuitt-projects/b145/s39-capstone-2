const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');



// Add a product
router.post('/addproduct', auth.verify, (req, res) => {
	
	const data = {
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
});


// Get all active products
router.get('/getactive', auth.verify, (req, res) => {

	const data = {
		payload: auth.decode(req.headers.authorization)
	}

	productController.getAllActiveProducts(data).then(resultFromController => {
		res.send(resultFromController)
	})
});


// Get all inactive products
router.get('/getinactive', auth.verify, (req, res) => {

	const data = {
		payload: auth.decode(req.headers.authorization)
	}

	productController.getAllInactiveProducts(data).then(resultFromController => {
		res.send(resultFromController)
	})
});

// Get Product by category
router.get('/getproduct/:category', (req, res) => {
	
	const category = req.params.category;

	productController.productCategory(category).then(resultFromController => {
		res.send(resultFromController)
	})
});

// Get Specific Product

router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Update Product
router.put('/:productId', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization),
		updatedProduct : req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
});






module.exports = router;