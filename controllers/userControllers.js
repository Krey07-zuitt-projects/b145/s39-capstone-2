const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// User SIGNUP

module.exports.signUp = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return `Email already exisit`
		} else {
			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: bcrypt.hashSync(reqBody.password, 10)
			})	

			return newUser.save().then((user, err) => {
				if(err){
					return `Something went wrong`
				} else {
					return `You have successfully sign up!`
				}
			})
		}
	})
};

// USER SIGNIN

module.exports.signIn = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return `Incorrect Email`
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {
				return `Incorrect Password`
			}
		}
	})
};


// CHANGE TO ADMIN

module.exports.changeToAdmin = (data) => {
	
	return User.findById(data.userId).then((result,err) => {
		
		if(data.payload.isAdmin === true){
		
				result.isAdmin = data.updatedUser.isAdmin
				
			return result.save().then((updatedUser, err) => {
		
				if(err){
					return false
				} else {
					return updatedUser
				}
			})
		} else {
			return false
		}
	})
}


// GET ALL USERS

module.exports.getUser = async (data) => {
	if(data.payload.isAdmin === true){
		return User.find({}).then(result => {
			return result
		})
	} else {
		return `User is not authorized`
	}
};
	
