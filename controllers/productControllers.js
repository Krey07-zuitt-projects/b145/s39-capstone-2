const Product = require('../models/Product');
const User = require('../models/User');


// Add a Product

module.exports.addProduct = async (data) => {
	const {productName, description, category, price, isActive, quantity} = data.reqBody

	let isProductExist = await Product.find({productName: productName}).then(result => {
		if(result.length > 0) {
			return true;
		}
		else {
			return false;
		}
	})

	if(isProductExist) {
		return `This product already exist in your inventory`
	}

	if(data.payload.isAdmin === true) {
		let newProduct = new Product({
			productName: productName,
			description: description,
			category: category,
			price: price,
			isActive: isActive,
			quantity: quantity

		});

		return newProduct.save().then((product, err) => {
			if(err) {
				return false;
			}
			else {
				return product;
			}
		})
	}
	else {
		return `This user ${data.payload.email} is currently not an admin`;
	}
}

// Get Active Products

module.exports.getAllActiveProducts = async (data) => {
	if(data.payload.isAdmin === true){
		return Product.find({isActive: true}).then(result => {
			return result
		})
	} else {
		return `You are not an authorized user`
	}
}

// Get Inactive Products

module.exports.getAllInactiveProducts = async (data) => {
	if(data.payload.isAdmin === true){
		return Product.find({isActive: false}).then(result => {
			return result
		})
	} else {
		return `You are not an authorized user`
	}
};


// Get Product by category
module.exports.productCategory = (category) => {
	return Product.find({category: category}).then(result => {
		return result;
	})
};

// Get Specific Product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

// Update Product
module.exports.updateProduct = (data) => {

	return Product.findById(data.productId).then((result,err) => {

		if(data.payload.isAdmin === true){
		
				result.productName = data.updatedProduct.productName
				result.description = data.updatedProduct.description
				result.category = data.updatedProduct.category
				result.price = data.updatedProduct.price
				result.isActive = data.updatedProduct.isActive
				result.quantity = data.updatedProduct.quantity
				
			return result.save().then((updatedProduct, err) => {
		
				if(err){
					return false
				} else {
					return updatedProduct
				}
			})
		} else {
			return `You need to be an authorized user`
		}
	})
}
