const express = require('express');
const env = require('dotenv');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 4000;

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

env.config();
const secret = process.env.CONNECTION_STRING;

mongoose.connect(secret,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

app.get('/', (req, res) => {
	res.send('hosted in heroku')
})

app.listen(port, () => console.log(`My server is running successfully running at port ${port}`))